import React from 'react';
import { View, TextInput, Alert } from 'react-native';
import { connect } from "react-redux";
import {AsyncStorage} from 'react-native';
import styles from './style';
import Icon from 'react-native-vector-icons/Ionicons';
import OvalButton from '../../components/OvalButton';


class LoginContainer extends React.Component {

  constructor(props){
    super(props);
    this.dispatch = props.dispatch;        
  }

  state = {
    email: "",
    password: ""
  };

  //Faz a requisicao de login para obter um token
  postRequest = () => {
    fetch('https://api-app-focus.herokuapp.com/login', {
        method: "POST",
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            user: {
              email: this.state.email,
              password: this.state.password,
            }
        }),
    })
    .then((response) => response.json())
    .then((responseJson) => {
        if(responseJson.user == null){
          Alert.alert("A conta não existe ou a Senha é inválida.");
          return 
        }
        console.log(responseJson.jwt, responseJson.user.id);
        this.dispatch(this.setUser(responseJson.jwt, responseJson.user.id));
        this.setUserInfo(responseJson.jwt, responseJson.user.id);
    })
    .catch((error) => {
        console.log(error);
    });
  }

  setUser(token, id){
    return{
        type: 'SET_USER',
        id,
        token
    };
  }

  //Persiste token de sessao e o id do usuario
  setUserInfo = async (token, id) => {
    try {
        await AsyncStorage.setItem('TOKEN', String(token));
        await AsyncStorage.setItem('ID', String(id));
    } catch (error) {
      console.log(error);
    }
    this.props.navigation.navigate('App');        
  };

  render(){
    return(
      <View style={styles.view}>
        <View style={styles.viewInput}>
          <Icon name="ios-person" size={20} style={styles.icons} />
          <TextInput
              style={styles.input}
              name="text" 
              onChangeText={(text) => this.setState({email: text})} 
              autoCapitalize='none'
              placeholder="E-mail"
          />
        </View>

        <View style={styles.viewInput}>
          <Icon name="ios-lock" size={20} style={styles.icons} />
          <TextInput
              style={styles.input}
              name="text" 
              onChangeText={(text) => this.setState({password: text})} 
              secureTextEntry={true}
              placeholder="Senha"
          />
        </View>

        <OvalButton onPress={this.postRequest} text={"Entrar"} buttonWidth={250} />

      </View>
    );
  }
}

export default connect(state => ({state: state}))(LoginContainer);
