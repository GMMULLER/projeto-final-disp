import React from 'react';
import { View } from 'react-native'; 
import LoginContainer from './LoginContainer';
import styles from './style';
import OvalButton from '../../components/OvalButton';
import general from '../../src/styles/general';
import Logo from '../../components/Logo';

export default class LoginScreen extends React.Component {
  
  static navigationOptions = {
    title: 'Entrar',
    header: null
  };

  navSignUp = () => {
    this.props.navigation.navigate('SignUp');
  }

  render(){
    return(
      <View style={general.containerCentralized}>
        <View style={styles.logo}>
          <Logo />
        </View>

        <LoginContainer navigation={this.props.navigation}/>

        <OvalButton onPress={this.navSignUp} text={"Cadastrar"} buttonWidth={250} />
      </View>
    );
  }

}