import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    view: {
        flexDirection: 'column',
        alignItems: 'center', 
    },
    viewInput: {
        flexDirection: "row",
        height: 35,
        borderBottomWidth: 1,
        borderBottomColor: '#0000cc',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
    },
    input: {
        flex: 1,
    },
    icons: {
        marginLeft: 6,
        marginRight: 6,
        marginTop: 5,
        marginBottom: 5,
    },
    logo: {
        marginBottom: 30
    }
});