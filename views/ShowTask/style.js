import { StyleSheet } from 'react-native';

export default StyleSheet.create({  
  textTitle: {
    fontSize: 18,
  },
  viewIconText: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5,
  },
  textIconText: {
    marginLeft: 5,
  },
  section: {
    marginTop: 5, 
    marginBottom: 5, 
    backgroundColor: '#F5F4F4',
    padding: 5,
  },
  minHeight: {
    minHeight: 80,
  },
  table: {
    marginTop: 10,
    marginBottom: 10,
  },
  underline: {
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0, 0, 0, 0.1)',
  },
  textType: {
    fontSize: 17,
    // fontFamily: 'Roboto',
    marginRight: 7,
  },
  textWrap: {
    flex: 1, 
    flexWrap: 'wrap'
  }
});