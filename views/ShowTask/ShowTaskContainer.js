import React from 'react';
import { View, Text, FlatList } from 'react-native';
import {AsyncStorage} from 'react-native';
import styles from './style';
import IconFeather from 'react-native-vector-icons/Feather';
import ProgressBar from '../../components/ProgressBar';


export default class ShowTaskContainer extends React.Component{

    state = {
        isLoading: true,
        notesSource: []
    };

    //Faz a requisicao para buscar informacoes de uma tarefa especifica
    getTask = (token) => {
        fetch('https://api-app-focus.herokuapp.com/tasks/' + this.props.task_id, {
            method: "GET",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+token
            },
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({
                //Parando o botão de loading
                isLoading: false,
                title: responseJson.title,
                goal: responseJson.goal,
                ask: responseJson.ask,
                answer: responseJson.answer,
                conclusion: responseJson.conclusion,
                total_time: responseJson.total_time,
                difficulty: responseJson.difficulty,
                user_id: responseJson.id,
                finished: responseJson.finished,
            });
        })
        .catch((error) =>{
            console.error(error);
        });
    }

    //Faz a requisicao para pegar as anotacoes atreladas a uma tarefa terminada
    getContent = async () => {
        const token = await AsyncStorage.getItem('TOKEN');

        fetch('https://api-app-focus.herokuapp.com/index_notes/' + this.props.task_id, {
            method: "GET",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+token
            },
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({notesSource: responseJson})
        })
        .catch((error) =>{
            console.error(error);
        });

        this.getTask(token);
        this.setState({isLoading: false});
        
    }

    componentDidMount() {
        this.getContent();

        this.focusListener = this.props.navigation.addListener('didFocus', () => this.getContent());
    }

    componentWillUnmount() {
        this.focusListener.remove();
    }

    //Tranforma o valor inteiro de dificuldade para um string
    transform_difficulty_to_string = (difficulty) => {
        if(difficulty == 0) {
          return "Fácil"
        } else if(difficulty == 1) {
          return "Médio"
        } else if(difficulty == 2) {
          return "Difícil"
        }
    }

    //Formata uma quantidade um segundos para um valor legivel
    transform_total_time = (total_time) => {
        if(total_time >= 3600) {
            hours = parseInt(total_time / 3600);
            restHours = total_time % 3600;
            if(restHours >= 60) {
                minutes = parseInt(restHours / 60);
                seconds = restHours % 60;
                return hours + " hora(s), " + minutes + " minuto(s) e " + seconds + " segundo(s)"
            } else {
                seconds = restHours % 60;
                return hours + " hora(s), " + 0 + "minuto(s) e " + seconds + " segundo(s)"
            }
        } else if(total_time >= 60 && total_time < 3600) {
            minutes = parseInt(total_time / 60);
            seconds = total_time % 60;
            return minutes + " minuto(s) e " + seconds + " segundo(s)"
        } else if(total_time >= 0 && total_time < 60) {
            return total_time + " segundo(s)"
        }
    }

    //Decide se deve mostrar o numero de horas dedicadas a tarefa
    print_total_time_or_not_done = () => {
        if(this.state.finished) {
            return this.transform_total_time(this.state.total_time)
        } else {
            return 'Não realizada'
        }
    }

    render(){

        if(this.state.isLoading){
            return(
                <View style={{flex:1, padding:20}}>
                    <Text>Loading...</Text>
                </View>
            );
        }

        return(
            <View>
                <View style={styles.section}>
                    <View style={{flexDirection: 'row', borderBottomWidth: 1, borderBottomColor: 'gray', alignItems: 'center'}}>
                        <Text style={styles.textTitle}>{this.state.title}</Text>
                        <View style={{flex: 1, alignItems: 'flex-end'}}>
                            <Text>{this.print_total_time_or_not_done()}</Text>
                        </View>
                    </View>
                    <View style={[styles.viewIconText, {marginTop: 5, marginBottom: 5}]}>
                        <View style={[styles.viewIconText, {flex: 1, justifyContent: 'center',}]}>
                            <Text>Dificuldade:</Text>
                            <ProgressBar progressNumber={this.state.difficulty+1} />
                        </View>
                    </View>
                </View>
                

                <View style={styles.section}>
                    <View style={styles.table}>
                        <View style={[styles.viewIconText, styles.underline]}>
                            <Text style={[styles.textType, styles.textWrap, {flex:0.4}]}>Objetivo</Text>
                            <Text style={styles.textWrap}>{this.state.goal}</Text>
                        </View>
                        <View style={[styles.viewIconText, styles.underline]}>
                            <Text style={[styles.textType, styles.textWrap, {flex:0.4}]}>Conclusão</Text>
                            <Text style={styles.textWrap}>{this.state.conclusion}</Text>
                        </View>
                    </View>
                    <View style={styles.table}>
                        <View style={[styles.viewIconText, styles.underline]}>
                            <Text style={[styles.textType, styles.textWrap, {flex:0.4}]}>Pergunta</Text>
                            <Text style={styles.textWrap}>{this.state.ask}</Text>
                        </View>
                        <View style={[styles.viewIconText, styles.underline]}>
                            <Text style={[styles.textType, styles.textWrap, {flex:0.4}]}>Resposta</Text>
                            <Text style={styles.textWrap}>{this.state.answer}</Text>
                        </View>
                    </View>
                </View>

                <View style={[styles.section, styles.minHeight]}>
                    <View style={{alignItems: 'center',}}>
                        <Text style={{fontSize: 15,}}>Notas</Text>
                    </View>

                    <FlatList
                        data={this.state.notesSource}
                        renderItem={({ item }) => <Text>{item.content}</Text>}
                        keyExtractor={(item) => {
                            return item.id.toString();
                        }}
                    />
                </View>
            </View>
        );
    }
}