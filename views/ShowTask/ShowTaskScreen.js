import React from 'react';
import { KeyboardAvoidingView, View } from 'react-native';
import ShowTaskContainer from "./ShowTaskContainer";
import { ScrollView } from 'react-native-gesture-handler';
import general from "../../src/styles/general";
import OvalButton from "../../components/OvalButton";

export default class ShowTaskScreen extends React.Component {

  state = {
  };
  
  static navigationOptions = {
    title: 'Tarefa',
  };

  //Verifica se eh para renderizar o botao de iniciar a tarefa
  print_botao_iniciar_tarefa() {
    task_finished = this.props.navigation.state.params.task_finished;
    topic_finisher = this.props.navigation.state.params.topicDone;
    if(!task_finished && !topic_finisher) {
      return(
        <OvalButton onPress={() => this.props.navigation.navigate('Timer', {
          task_id: this.task_id,
          ask: this.props.navigation.state.params.ask
        })} text={"Iniciar"} buttonWidth={250} />
      );
    }
  }

  render(){
    this.task_id = this.props.navigation.state.params.task_id;
    return(
      <View style={general.container}>
        <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column', justifyContent: 'center'}} behavior="padding" keyboardVerticalOffset={100}>
          <ScrollView>
              <ShowTaskContainer navigation={this.props.navigation} task_id={this.task_id}/>

              {this.print_botao_iniciar_tarefa()}              
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }

}