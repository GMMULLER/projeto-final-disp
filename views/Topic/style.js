import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  viewTopic: {
    justifyContent: 'center',
    marginTop: 3,
    marginBottom: 3,
    backgroundColor: '#F5F4F4',
    height: 50,
  },
  viewInfo: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginRight: 10,
    marginLeft: 10,
  },
  topicTitle: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  topicPercents: {
    justifyContent: 'flex-start',
  },
  text: {
    fontSize: 16,
  },
});