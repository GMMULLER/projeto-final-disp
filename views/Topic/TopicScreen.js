import React from 'react';
import { View } from 'react-native';
import general from "../../src/styles/general";
import TopicContainer from "./TopicContainer";
import { StyleSheet } from 'react-native';
import LogoTitle from '../../components/LogoTitle';

export default class TopicScreen extends React.Component {

    static navigationOptions = {
        headerTitle: () => <LogoTitle />,
    };

    render(){
      return(
        <View style={general.container}>
          <TopicContainer navigation={this.props.navigation} />
        </View>
      );
    }
}

const styles = StyleSheet.create({
    buttonPos: {
      position:'absolute',
      bottom:80, 
      right: 20, 
      alignSelf:'flex-end',
    },
    buttonPos2: {
        position:'absolute',
        bottom:160, 
        right: 20, 
        alignSelf:'flex-end',
    },
});