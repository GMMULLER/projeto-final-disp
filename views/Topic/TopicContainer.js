import React from 'react';
import { View, Text, ScrollView, TouchableOpacity } from 'react-native';
import { AsyncStorage } from 'react-native';
import styles from './style';
import Swipeout from 'react-native-swipeout';
import AddItemButton from '../../components/AddItemButton';

export default class TopicContainer extends React.Component {

    constructor(props){
        super(props);
        this.state = { isLoading: true }
    }

    state = {
        loading: true,
        error: false,
        topics: [],
    };

    //Adiciona um listener que atualizara os topicos sem que essa tela for focada
    componentDidMount(){
        this.setState({
            isLoading: true
        })
        this.focusListener = this.props.navigation.addListener('didFocus', () => this.getTopicsRequest());
    }

    componentWillUnmount(){
        this.focusListener.remove();
    }

    //Função que retorna todos os assuntos que o usuário logado tem
    getTopicsRequest = async () => {
        try {
            const token = await AsyncStorage.getItem('TOKEN');
            const id = await AsyncStorage.getItem('ID');

            return fetch('https://api-app-focus.herokuapp.com/topics', { headers: {"Authorization": "Bearer " + token, "Content-Type":"application/json" }})
                .then((response) => response.json())
                .then((responseJson) => {
                    this.setState({
                        isLoading: false,
                        dataSource: responseJson,
                    }, function(){
    
                    });
                })
                .catch((error) =>{
                    console.error(error);
                });

        } catch (error) {
            console.log(error);
        }
    }

    //Faz a requisicao para recuperacao de um assunto
    deleteRequest = async (topicId) => {
        try {
            const token = await AsyncStorage.getItem('TOKEN');
            const id = await AsyncStorage.getItem('ID');
    
            fetch('https://api-app-focus.herokuapp.com/topics/' + topicId, {
                method: "DELETE",
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': "Bearer " + token
                },
            })
            .then((response) => {
                this.getTopicsRequest();
            })
            .catch((error) =>{
              console.error(error);
            });
        } catch (error) {
            console.log(error);
        }

    }

    //Atualiza um assunto para finaliza-lo
    updateRequestDone = async (topicId) => {
        const token = await AsyncStorage.getItem('TOKEN');
        const id = await AsyncStorage.getItem('ID');

        fetch('https://api-app-focus.herokuapp.com/topics/' + topicId, {
            method: "PUT",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+token
            },
            body: JSON.stringify({
                done: true,
            }),
        })

        auxDataSource = this.state.dataSource;
        auxDataSource.forEach((topic) => {
            if(topic.id == topicId) {
                topic.done = true;
            }
        });

        this.setState({dataSource: auxDataSource});
    }

    //Função que transforma as horas para uma string legível para o usuário
    transform_seconds_in_hours = (total_time) => {
      if(total_time >= 3600) {
          hours = parseInt(total_time / 3600);
          restHours = total_time % 3600;
          if(restHours >= 60) {
              minutes = parseInt(restHours / 60);
              seconds = restHours % 60;
              return hours + " hora(s), " + minutes + " minuto(s) e " + seconds + " segundo(s)"
          } else {
              seconds = restHours % 60;
              return hours + " hora(s), " + 0 + "minuto(s) e " + seconds + " segundo(s)"
          }
      } else if(total_time >= 60 && total_time < 3600) {
          minutes = parseInt(total_time / 60);
          seconds = total_time % 60;
          return minutes + " minuto(s) e " + seconds + " segundo(s)"
      } else if(total_time >= 0 && total_time < 60) {
          return total_time + " segundo(s)"
      }
    }

    //Retorna quantos por cento do assunto foram completos
    task_completed_percentage = (seconds_performed, amount_planned_seconds_per_week, topicDone) => {
        if(topicDone){
            return 'Assunto Finalizado';
        }
        if(amount_planned_seconds_per_week == null || amount_planned_seconds_per_week == 0) {
            return 'Sem Horas Planejadas';
        }
        percentage = parseInt((seconds_performed/amount_planned_seconds_per_week) * 100);
        if(percentage > 100) {
            percentage = 100;
        }
        return (percentage).toString() + "%";
    }

    render() {

        if(this.state.isLoading){
            return(
              <View style={{flex: 1, padding: 20}}>
                <Text>Loading...</Text>
              </View>
            )
        }

        return(

            <ScrollView style={{flex: 1}}>

                {this.state.dataSource.map((topic) => {
                    return(
                        <View style={styles.viewTopic} key={topic.id} >
                            <Swipeout right={
                                [
                                    {
                                        text: 'Apagar',
                                        color: '#800080',
                                        backgroundColor: '#F5F4F4',
                                        onPress: () => { this.deleteRequest(topic.id) }
                                    },
                                    {
                                        text: 'Finalizar',
                                        color: '#800080',
                                        backgroundColor: '#F5F4F4',
                                        onPress: () => { this.updateRequestDone(topic.id) }
                                    }
                                ]
                            }
                                backgroundColor='transparent'
                            >
                                <TouchableOpacity
                                    onPress={() => this.props.navigation.navigate("Task", {
                                        topicId: topic.id,
                                        done: topic.done,
                                        topicTitle: topic.title
                                    })}
                                >
                                    <View style={styles.viewInfo}>
                                        <View style={styles.topicTitle}>
                                            <Text style={styles.text}>{topic.title}</Text>
                                        </View>
                                        <View style={styles.topicPercents}>
                                            <Text style={styles.text}>{this.task_completed_percentage(topic.seconds_performed, topic.amount_planned_seconds_per_week, topic.done)}</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </Swipeout>
                        </View>
                    );
                })}
                <AddItemButton navigation={this.props.navigation} goTo={"NewTopic"} buttonContent={"+ Criar Assunto"} />
            </ScrollView>
        );
    }

}
