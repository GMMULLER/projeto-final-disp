import React from 'react';
import { View, TextInput, Text, TouchableOpacity } from 'react-native';
import { connect } from "react-redux";
import { AsyncStorage } from 'react-native';
import { stylesNotes } from './style';
import { ScrollView } from 'react-native-gesture-handler';

class ShowNotesContainer extends React.Component{
    
    constructor(props){
        super(props);
        this.dispatch = props.dispatch;
    }

    state = {
        isLoading: true,
        createdNote: "",
        notesSource: []
    };

    setNotes(remoteNotes, taskId){
        return{
            type: 'SET_NOTES',
            remoteNotes,
            taskId
        };
    }

    appendNote(note){
        return{
            type: 'APPEND_NOTE',
            note
        };
    }

    //Faz a requisicao para recuperar todas as anotacoes da tarefa
    getNotes = async () => { //Deve atualizar o estado local
        const token = await AsyncStorage.getItem('TOKEN');
        const id = await AsyncStorage.getItem('ID');

        fetch('https://api-app-focus.herokuapp.com/index_notes/' + this.props.task_id, {
            method: "GET",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+token
            },
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({notesSource: responseJson})
        })
        .catch((error) =>{
            console.error(error);
        });

        if(this.props.noteSystem.taskId != this.props.task_id){ //Se esta for a primeira vez no cronometro dessa tarefa atualiza as anotacoes globalmente pelo Redux
            this.dispatch(this.setNotes(this.state.notesSource, this.props.task_id));
        }
    }

    componentWillMount(){
        this.getNotes();
    }

    //Cria uma nova anotacao pra esta tarefa
    postNote = async () => {
        const token = await AsyncStorage.getItem('TOKEN');
        const id = await AsyncStorage.getItem('ID');

        try{
            fetch('https://api-app-focus.herokuapp.com/notes', {
                method: "POST",
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer '+token
                },
                body: JSON.stringify({
                    content: this.state.createdNote,
                    task_id: parseInt(this.props.task_id)
                }),
            })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
            })
            .catch((error) =>{
                console.error(error);
            });
        }catch(error){
            console.log(error)
        }
    }

    //Atualiza as anotacoes da tarefa corrente localmente, globalmente e remotamente
    attNote = () => { 
        this.setState({notesSource:[...this.state.notesSource, {content:this.state.createdNote}]});
        this.dispatch(this.appendNote(this.state.createdNote));
        this.postNote();
        this.forceUpdate();
    }

    render(){
        return(
            <View>
                <View style={stylesNotes.viewInputNotes}>
                    <TextInput 
                        multiline={true}
                        name="text" 
                        onChangeText={(text) => this.setState({createdNote: text})} 
                        placeholder="Anotações"
                    />
                </View>

                <View style={stylesNotes.view}>
                    <TouchableOpacity
                        onPress={this.attNote}
                    >
                        <Text style={stylesNotes.text}>+ Criar Nota</Text>
                    </TouchableOpacity>
                </View>

                <ScrollView>    
                    <View style={stylesNotes.viewNotes}>       
                        {
                            this.state.notesSource.map((note, i) => {
                                return(
                                    <View key={i} style={stylesNotes.viewNote}>
                                        <Text>{note.content}</Text>
                                    </View>
                                );
                            })
                        }
                    </View>
                </ScrollView>

            </View>
        );
    }
}

export default connect(state => ({noteSystem: state.noteSystem}))(ShowNotesContainer);