import React from 'react';
import { View, Text } from 'react-native'; 
import Timer from '../../components/Timer';
import { connect } from "react-redux";
import ShowNotesContainer from "./ShowNotesContainer";
import ConclusionModal from "./ConclusionModal";
import { AsyncStorage } from 'react-native';
import general from "../../src/styles/general";
import styles from './style';
import Icon from 'react-native-vector-icons/Ionicons';

class TimerContainer extends React.Component {

    constructor(props){
        super(props);
        this.dispatch = props.dispatch;
    }

    state = {
        interval_time: null,
        duration_time: null
    };

    attTimer(){
        return {
            type: 'ATT_TIMER'
        };
    }

    setTimer(interval){
        return{
            type: 'SET_TIMER',
            interval
        };
    }

    clearTimer(){
        return{
            type: 'CLEAR_TIMER'
        };
    }

    setFocused(){
        return{
            type: 'SET_FOCUSED'
        };
    }

    unsetFocused(){
        return{
            type: 'UNSET_FOCUSED'
        };
    }

    checkBreak(){
        var intervalDurationSec = this.state.duration_time;
        var intervalTimeSec = this.state.interval_time;

        return{
            type: 'CHECK_BREAK',
            intervalDurationSec,
            intervalTimeSec
        };
    }

    updateTime = () => {
        this.dispatch(this.attTimer());
        this.dispatch(this.checkBreak());
    }

    componentWillUpdate() {
        this.timer = this.props.timer.counter;
    }

    componentDidMount() {
        this.startTimer();
        this.getUserInfo();
        this.dispatch(this.setFocused());        
    }

    componentWillUnmount(){
        this.dispatch(this.unsetFocused());
    }

    //Inicializa o timer
    startTimer(){
        this.timer = this.props.timer.counter;
        this.setState({
            task_id: this.props.navigation.state.params.task_id
        });
        if(!this.props.timer.running){ //Se eh a primeira vez na tela de cronometro da tarefa
            this.interval = setInterval(() => { //Cria um intervalo que atualizara o cronometro a cada um segundo
                this.updateTime();
            }, 1000);
            this.dispatch(this.setTimer(this.interval));
        }
    }

    //Formata a string de hora
    manipulation_string_h(hour) {
        if(hour < 10)
            return "0" + hour;
        else
            return hour;
    }

    //Formata a string de minuto
    manipulation_string_m(min) {
        if(min < 10)
            return "0" + min;
        else
            return min;
    }

    //Formata a string de segundo
    manipulation_string_s(sec) {
        if(sec < 10)
            return "0" + sec;
        else
            return sec;
    }

    //Recupera as informacoes de tempo e duracao do intervalo
    getUserInfo = async () => {
        const token = await AsyncStorage.getItem('TOKEN');
        const id = await AsyncStorage.getItem('ID');
        
        fetch('https://api-app-focus.herokuapp.com/users/' + id, {
            method: "GET",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+token,
            },
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({
                interval_time: responseJson.interval_time,
                duration_time: responseJson.duration_time
            })
        })
        .catch((error) =>{
            console.error(error);
        });   
    }

    //Faz a requisicao para finalizar a tarefa
    finish_task = async (conclusion_text, answer_text) => {
        time = this.props.timer.counter.s + this.props.timer.counter.m * 60 + this.props.timer.counter.h * 3600;
        taskId = this.props.navigation.state.params.task_id;

        const token = await AsyncStorage.getItem('TOKEN');

        fetch('https://api-app-focus.herokuapp.com/tasks/' + taskId, {
            method: "PUT",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+token
            },
            body: JSON.stringify({
                total_time: time,
                finished: true,
                conclusion: conclusion_text,
                answer: answer_text
            }),
        })
        this.dispatch(this.clearTimer());
        this.props.navigation.navigate('Topic'); //Volta para a tela de assuntos 
        
    }

    //Checa se deve mostrar o lembrete de beber agua
    checkWater(){
        time = this.props.timer.counter.s + this.props.timer.counter.m * 60 + this.props.timer.counter.h * 3600;
        if(time%20 >= 0 && time%20 <= 10 && time>=20){ //A cada 20 segundos por 10 segundos
            return true;
        }
        return false;
    }

    render(){
        return(
            <View style={general.container}>
                <View style={styles.view_timer}>
                    <Timer style={this.props.timer.isInBreak ? styles.interval_timer : styles.normal_timer} h={this.manipulation_string_h(this.props.timer.counter.h)} m={this.manipulation_string_m(this.props.timer.counter.m)} s={this.manipulation_string_s(this.props.timer.counter.s)} />
                </View>

                <View style={styles.view_text}>
                    <Text style={this.props.timer.isInBreak ? styles.show_text : styles.hide_text}>Intervalo!</Text>
                </View>

                <View style={this.checkWater() ? styles.show_text : styles.hide_text}>
                    <View style={styles.view_text}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <Icon name="ios-water" size={27} style={{marginRight: 3}}/>
                            <Text>Não se esqueça de beber água!</Text>
                        </View>
                    </View>
                </View>

                <View style={styles.viewModal}>
                    <ConclusionModal closeModal={this.finish_task} restartTimer={this.startTimer} ask={this.props.navigation.state.params.ask}/>
                </View>

                <View style={styles.viewNotes}>
                    <ShowNotesContainer task_id={this.props.navigation.state.params.task_id}/>
                </View>
            </View>
        );
    }
}

export default connect(state => ({timer: state}))(TimerContainer);