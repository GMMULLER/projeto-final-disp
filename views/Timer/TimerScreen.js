import React from 'react';
import { View } from 'react-native';
import general from "../../src/styles/general";
import TimerContainer from "./TimerContainer";

export default class TimerScreen extends React.Component {

    static navigationOptions = {
        title: 'Cronômetro',
    };

    render(){
        return(
            <View style={general.container}>
                <TimerContainer navigation={this.props.navigation} />
            </View>
        );
    }
}