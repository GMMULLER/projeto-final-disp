import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    view_timer: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    view_notes: {
        flexDirection: 'column',
        justifyContent: 'center',
    },
    view_modal: {
        flexDirection: 'column',
    },
    interval_timer: {
        color: '#cc0000',
        fontSize: 60
    },
    normal_timer: {
        color: '#000000',
        fontSize: 60      
    },
    show_text: {
        color: '#cc0000',
        fontSize: 20
    },
    hide_text: {
        display: 'none'
    },
    view_text: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    }
});

export const stylesNotes = StyleSheet.create({
    view: {
        alignItems: 'center',
        margin: 10,
    },
    text: {
        fontSize: 16,
        color: '#800080',
        fontWeight: 'bold',
    },
    viewInputNotes: {
        borderWidth: 1,
        borderColor: '#0000cc',
        borderRadius: 20,
        marginTop: 20,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        paddingLeft: 5,
        paddingRight: 5,
        paddingTop: 0,
        paddingBottom: 2,
    }, 
    viewNotes: {
        marginTop: 10,
        marginBottom: 10,
    },
    viewNote: {
        backgroundColor: "#F5F4F4",
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 4,
        paddingBottom: 4,
        marginBottom: 5, 
    }
});

export const stylesModal = StyleSheet.create({
    viewTitle: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 5,
    },
    title: {
        fontSize: 15,
    },
    viewTextInput: {
        borderWidth: 1,
        borderColor: '#0000cc',
        borderRadius: 20,
        marginTop: 10,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        paddingLeft: 5,
        paddingRight: 5,
        paddingTop: 0,
        paddingBottom: 2,
    },
});