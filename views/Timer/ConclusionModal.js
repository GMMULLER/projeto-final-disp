import React, {Component} from 'react';
import {Modal, Text, View, TextInput} from 'react-native';
import { connect } from "react-redux";
import OvalButton from '../../components/OvalButton';
import { stylesModal } from './style'

class ConclusionModal extends Component {
  
  constructor(props){
    super(props);
    this.dispatch = props.dispatch;
  }

  state = {
    modalVisible: false,
    conclusion: "",
    answer: "",
  };

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  //Abre o modal e pausa o cronometro
  openModal = () => {
    this.setModalVisible(true);
    this.dispatch(this.stopTimer());
  }

  //Fecha o modal e chama a funcao do componente pai
  closeModal = () => {
    this.setModalVisible(!this.state.modalVisible);
    this.props.closeModal(this.state.conclusion, this.state.answer);
  }

  stopTimer(){
    return{
        type: 'STOP_TIMER'
    };
  }

  render() {
    return (
      <View style={{marginTop: 22}}>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
        >
          <View style={{marginTop: 50}}>

            <View style={stylesModal.viewTitle}>
              <Text style={stylesModal.title}>O que você concluiu com essa tarefa?</Text>
            </View>

            <View style={stylesModal.viewTextInput}>
              {/* Modal Content */}
              <TextInput 
                    multiline={true}
                    name="conclusion" 
                    onChangeText={(text) => this.setState({conclusion: text})} 
                    placeholder="O que você concluiu com essa tarefa?"
              />
            </View>

            <View style={stylesModal.viewTitle}>
              <Text style={stylesModal.title}>{this.props.ask}</Text>
            </View>

            <View style={stylesModal.viewTextInput}>
              <TextInput 
                multiline={true}
                name="answer" 
                onChangeText={(text) => this.setState({answer: text})} 
                placeholder="Resposta"
              />
            </View>

            <OvalButton onPress={this.closeModal} text={"Confirmar"} buttonWidth={250} />

          </View>
        </Modal>

        <OvalButton onPress={this.openModal} text={"Finalizar"} buttonWidth={250} />

      </View>
    );
  }
}

export default connect(state => ({timer: state}))(ConclusionModal);
