import React from 'react';
import { ScrollView, Dimensions, View } from 'react-native';
import { AsyncStorage } from 'react-native';
import { ProgressChart } from "react-native-chart-kit";

export default class GraphicContainer extends React.Component{

    state={
        progressData: {
            labels: [],
            data: []
        },
        chartConfig: {
            backgroundGradientFrom: "#ffffff",
            backgroundGradientFromOpacity: 0,
            backgroundGradientTo: "#ffffff",
            backgroundGradientToOpacity: 0.5,
            color: (opacity = 1) => `rgba(121, 0, 158, ${opacity})`,
            strokeWidth: 2, // optional, default 3
            barPercentage: 0.5
        },
    };

    //Retorna quantos por cento do assunto foram completados na semana
    task_completed_percentage = (seconds_performed, amount_planned_seconds_per_week) => {
        if(amount_planned_seconds_per_week == null || amount_planned_seconds_per_week == 0) {
            return 0;
        }
        percentage = seconds_performed/amount_planned_seconds_per_week;
        percentage = Math.round(percentage * 100) / 100
        if(percentage > 1) {
            percentage = 1;
        }
        return percentage;
    }
    
    //Requisicao para a recuperacao de todos os topicos na API
    getTopicsRequest = async () => {
        try {
            const token = await AsyncStorage.getItem('TOKEN');

            return fetch('https://api-app-focus.herokuapp.com/topics', { headers: {"Authorization": "Bearer " + token, "Content-Type":"application/json" }})
                .then((response) => response.json())
                .then((responseJson) => {

                    this.setState({progressData: { //Resetando os valores dos states
                        labels: [],
                        data: []
                    }});

                    responseJson.forEach((topic) => { //Formata os dados para o grafico 
                        this.setState({progressData: {
                            labels: [...this.state.progressData.labels, topic.title],
                            data: [...this.state.progressData.data, this.task_completed_percentage(topic.seconds_performed, topic.amount_planned_seconds_per_week)]
                        }});
                    });
                })
                .catch((error) =>{
                    console.error(error);
                });

        } catch (error) {
            console.log(error);
        }
    }

    //Adiciona um listener que vai atualizar os topicos todas as vezes que a pagina for focada
    componentDidMount(){
        this.focusListener = this.props.navigation.addListener('didFocus', () => this.getTopicsRequest());
    }

    //Remove o listener
    componentWillUnmount(){
        this.focusListener.remove();
    }

    render(){

        return(
            <ScrollView>
                <View style={{marginTop: 10, marginLeft: -20}}>
                    <ProgressChart
                        data={this.state.progressData}
                        width={Dimensions.get("window").width}
                        height={260}
                        chartConfig={this.state.chartConfig}
                    />
                </View>
            </ScrollView>
        );

    }
}