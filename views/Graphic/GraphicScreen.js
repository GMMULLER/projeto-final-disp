import React from 'react';
import { View, Text } from 'react-native'; 
import { connect } from "react-redux";
import GraphicContainer from "./GraphicContainer";

class GraphicScreen extends React.Component {
    render(){
        return(
          <View>
            <GraphicContainer navigation={this.props.navigation}/>              
          </View>
        );
    }
}

export default connect(state => ({state: state}))(GraphicScreen);