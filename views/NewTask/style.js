import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    view: {
        flex: 1,
        marginTop: 30,
    },
    viewTitle: {
        alignItems: 'center'
    },
    title: {
        fontSize: 20,
    },  
    viewInput: {
        height: 40,
        flexDirection: 'column',
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#800080',
        marginTop: 5,
        marginRight: 10,
        marginBottom: 5,
        marginLeft: 10,
    },
    chosenTopics: {
        backgroundColor: '#F5F4F4',
        padding: 5,
        marginRight: 7,
        marginTop: 3,
        marginBottom: 4,
        flexDirection: 'row'
    },
    chosenTopicsText: {
        width: 100,
    }
});