import React from 'react';
import { View, TextInput, Text, ScrollView } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import {AsyncStorage} from 'react-native';
import styles from './style';
import OvalButton from '../../components/OvalButton';
import Icon from 'react-native-vector-icons/Ionicons';

export default class NewTaskContainer extends React.Component{
    
    state = {
        isLoading: true,
        title: "",
        goal: "",
        ask: "",
        conclusion: "",
        answer: "",
        notes: "",
        total_time: 0,
        difficulty: 0,
        interval_time: 0,
        duration_time: 0,
        user_id: 1,
        finished: false,
        interval_text: "",
        duration_text: "",
        topics: [],
        chosenTopics: [],
        defaultTopicId: 0
    };

    //Faz a requisicao para a criacao de uma nova tarefa
    postRequest = async () => {
        const token = await AsyncStorage.getItem('TOKEN');

        auxTopicIds = [];
        this.state.chosenTopics.forEach((topic) => { //Copia os topicos escolhidos para um vetor auxiliar
            auxTopicIds.push(topic.id);
        });

        if(auxTopicIds.length == 0){ //Se o array estah vazio tem que pertencer ao sem assunto
            auxTopicIds.push(this.state.defaultTopicId); //Adiciona o id do assunto sem assunto
        }

        fetch('https://api-app-focus.herokuapp.com/tasks', {
            method: "POST",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+token
            },
            body: JSON.stringify({
                title: this.state.title,
                goal: this.state.goal,
                ask: this.state.ask,
                answer: this.state.answer,
                conclusion: this.state.conclusion,
                total_time: this.state.total_time,
                difficulty: this.state.difficulty,
                finished: this.state.finished,
                topic_ids: auxTopicIds
            }),
        })
        this.props.navigation.navigate('Topic');
    }

    //Valida a dificuldade inserida pelo usuario
    setDificulty = (value) => {
        if(value == null){ //Se nenhuma dificuldade foi escolhida
            this.setState({difficulty: 0});
        }else{
            this.setState({difficulty: value});
        }
    }

    //Adiciona mais um assunto a tarefa
    setTopic = (value) => {
        var alreadyAdded = false;
        this.state.chosenTopics.forEach((topic) => { //Verifica se o topico ja foi adicionado
            if(topic.id == value){
                alreadyAdded = true;
            }
        });

        if(value != null && !alreadyAdded){
            this.state.topics.forEach((option) => { //Percorre o array de todos os topicos para achar o nome
                if(option.value == value){ //Adiciona o topico escolhido
                    this.setState({chosenTopics: [...this.state.chosenTopics, {id:option.value, text:option.label}]});
                }
            });
        }
    }

    componentDidMount(){
        this.getTopics();
    }

    //Recupera todos os topicos para pertimir a escolha do usuario
    getTopics = async () =>{
        const token = await AsyncStorage.getItem('TOKEN');

        fetch('https://api-app-focus.herokuapp.com/topics/', {
            method: "GET",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+token
            },
        })
        .then((response) => response.json())
        .then((responseJson) => {
            var auxTopics = [];
            responseJson.forEach((topicInfo) => {
                if(!topicInfo.default_topic && !topicInfo.done){ //Se nao for um assunto default eh "escolhivel" pelo usuario e nao for um assunto finalizado
                    auxTopics.push({label:topicInfo.title, value:topicInfo.id});
                }else{ //Persiste qual eh o id do assunto default
                    this.setState({defaultTopicId: topicInfo.id});
                }
            });

            this.setState({
                isLoading: false,
                topics: auxTopics
            })
        })
        .catch((error) =>{
            console.error(error);
        });
    }

    //Remove um assunto da lista de escolhidos
    removeChosenTopic(topicId){
        auxTopics = this.state.chosenTopics;
        for(var i = 0; i < auxTopics.length; i++){
            if(auxTopics[i].id == topicId){
                auxTopics.splice(i, 1);
            }
        }
        this.setState({chosenTopics: auxTopics});
    }

    render(){
        if(this.state.isLoading){
            return(
              <View style={{flex: 1, padding: 20}}>
                <Text>Loading...</Text>
              </View>
            )
        }

        return(
            <ScrollView style={styles.view}>
                <View style={styles.viewTitle}>
                    <Text style={styles.title}>Nova Tarefa</Text>
                </View>
                <View style={styles.viewInput}>
                    <TextInput
                        style={styles.textInput}
                        name="text" 
                        onChangeText={(text) => this.setState({title: text})} 
                        placeholder="Título"
                    />
                </View>

                <View style={styles.viewInput}>
                    <TextInput
                        style={styles.textInput}
                        name="text" 
                        onChangeText={(text) => this.setState({goal: text})} 
                        placeholder="Objetivo"
                    />
                </View>

                <View style={styles.viewInput}>
                    <TextInput
                        style={styles.textInput}
                        name="text" 
                        onChangeText={(text) => this.setState({ask: text})} 
                        placeholder="Pergunta"
                    />
                </View>

                <View style={styles.viewInput}>
                    <RNPickerSelect
                        onValueChange={(value) => this.setDificulty(value)}
                        items={[
                            { label: 'Fácil', value: 0 },
                            { label: 'Médio', value: 1 },
                            { label: 'Difícil', value: 2 },
                        ]}
                        placeholder={{
                            label: "Selecione uma dificuldade",
                            value: null,
                        }}
                        style={{margin: 5}}
                    />
                </View>
                
                <View style={styles.viewInput}>
                    <RNPickerSelect
                        onValueChange={(topic) => this.setTopic(topic)}
                        items={this.state.topics}
                        placeholder={{
                            label: "Selecione um Assunto",
                            value: null, 
                        }}
                    />
                </View>

                <View style={{flexDirection: "row", marginLeft: 10, flexWrap: 'wrap', alignItems: "flex-start"}}>
                    {
                        this.state.chosenTopics.map((topic, i) => {
                            return(
                                <View key={i} style={styles.chosenTopics}>
                                    <Text numberOfLines={1} style={styles.chosenTopicsText}>
                                        {topic.text}
                                    </Text>
                                    <Icon onPress={() => this.removeChosenTopic(topic.id)} name="ios-backspace" size={20} styles={{justifyContent: 'flex-end'}}/>
                                </View>
                            );
                        })
                    }
                </View>

                <OvalButton onPress={this.postRequest} text={"Criar Tarefa"} buttonWidth={250} />
            </ScrollView>
        );
    }
}