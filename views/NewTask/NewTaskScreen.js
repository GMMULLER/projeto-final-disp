import React from 'react';
import { KeyboardAvoidingView, View } from 'react-native';
import NewTaskContainer from "./NewTaskContainer";
import { ScrollView } from 'react-native-gesture-handler';
import general from "../../src/styles/general";

export default class NewTaskScreen extends React.Component {

  state = {
    tasks: []
  };
  
  static navigationOptions = {
    title: 'Nova Tarefa',
  };

  render(){
    return(
      <View style={general.container}>
        <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column', justifyContent: 'center'}} behavior="padding" keyboardVerticalOffset={100}>
          <ScrollView>
            <NewTaskContainer navigation={this.props.navigation}/>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }

}
