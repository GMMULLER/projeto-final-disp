import React from 'react';
import { KeyboardAvoidingView, View } from 'react-native';
import NewTopicContainer from "./NewTopicContainer";
import { ScrollView } from 'react-native-gesture-handler';
import general from "../../src/styles/general";

export default class NewTopicScreen extends React.Component {

  state = {
    topics: []
  };
  
  static navigationOptions = {
    title: 'Novo Assunto',
  };

  render(){
    return(
      <View style={general.container}>
        <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column', justifyContent: 'center'}} behavior="padding" keyboardVerticalOffset={100}>
          <ScrollView>
            <NewTopicContainer navigation={this.props.navigation}/>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }

}
