import React from 'react';
import { View, TextInput, Text } from 'react-native';
import { TextInputMask } from 'react-native-masked-text'; 
import { AsyncStorage } from 'react-native';
import styles from './style';
import OvalButton from '../../components/OvalButton';

export default class NewTopicContainer extends React.Component{
    
    state = {
        title: "",
        amount_planned_seconds_per_week_text: "",
        amount_planned_seconds_per_week: 0,
        seconds_performed: 0
    };

    //Requisicao para a criacao de um novo assunto
    postRequest = async () => {
        try {
            const token = await AsyncStorage.getItem('TOKEN');
            const id = parseInt(await AsyncStorage.getItem('ID'));
            
            fetch('https://api-app-focus.herokuapp.com/topics', {
                method: "POST",
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': "Bearer " + token
                },
                body: JSON.stringify({
                    title: this.state.title,
                    amount_planned_seconds_per_week: this.state.amount_planned_seconds_per_week,
                    seconds_performed: this.state.seconds_performed,
                    user_id: id,
                }),
            }).then((response) => {
                console.log(JSON.stringify(response, null, 4))
            })
            this.props.navigation.navigate('Topic'); //Volta para a tela de assunto
        } catch (error) {
            console.log(error);
        }
    }

    //Converte a string de tempo planejado para segundos
    convertToSecondsI = (text) => {
        this.setState({amount_planned_seconds_per_week_text: text});
        var array = text.split(":");
        var hora = parseInt(array[0], 10);
        hora = hora * 60 * 60;
        var min = parseInt(array[1], 10);
        min = min * 60;
        var sec = parseInt(array[2], 10);
        sec = sec;

        var val = hora + min + sec;

        this.setState({amount_planned_seconds_per_week: val});
    };

    render(){
        return(
            <View style={styles.view}>
                <View style={styles.viewTitle}>
                    <Text style={styles.title}>Novo Assunto</Text>
                </View>
                <View style={styles.viewInput}>
                    <TextInput
                        style={styles.input}
                        name="text" 
                        onChangeText={(text) => this.setState({title: text})}
                        placeholder="Título" 
                    />
                </View>

                <View style={styles.viewInput}>
                    <Text style={styles.formLabel}>Tempo Planejado por Semana</Text>
                    <TextInputMask
                        style={styles.input}
                        type={'datetime'}
                        options={{
                            format: 'HH:mm:ss'
                        }}
                        value={this.state.amount_planned_seconds_per_week_text}
                        onChangeText={(text) => this.convertToSecondsI(text)} 
                        placeholder="23:23:23"
                    />
                </View>

                <OvalButton onPress={this.postRequest} text={"Criar Assunto"} buttonWidth={250} />

            </View>
        );
    }
}