import React from 'react';
import { View, KeyboardAvoidingView } from 'react-native';
import UpdateTaskContainer from "./UpdateTaskContainer";
import { ScrollView } from 'react-native-gesture-handler';

export default class UpdateTaskScreen extends React.Component {

  state = {
    tasks: []
  };
  
  static navigationOptions = {
    title: 'Atualizar Tarefa',
  };

  render(){
    task = this.props.navigation.state.params;
    return(
      <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column', justifyContent: 'center'}} behavior="padding" keyboardVerticalOffset={100}>
        <ScrollView>
          <UpdateTaskContainer navigation={this.props.navigation} infoTask = {task} />
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }

}