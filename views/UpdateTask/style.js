import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    view: {
        flex: 1,
        marginTop: 30,
    },
    viewTitle: {
        alignItems: 'center'
    },
    title: {
        fontSize: 20,
    },  
    form: {
        margin: 5,
        height: 40,
        borderColor: '#0000cc',
        borderWidth: 1,
        borderRadius: 25,
        padding: 10
    },
    formLabel: {
        margin: 5
    },
    button: {
        flex: 1,
        borderWidth:1,
        borderColor:'rgba(0,0,0,0.2)',
        alignItems:'center',
        justifyContent:'center',
        height:70,
        backgroundColor:'#800080',
    },
    viewInput: {
        height: 40,
        flexDirection: 'column',
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#800080',
        marginTop: 5,
        marginRight: 10,
        marginBottom: 5,
        marginLeft: 10,
    },
});