import React from 'react';
import { View, TextInput, Text, ScrollView } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import { AsyncStorage } from 'react-native';
import styles from './style';
import OvalButton from '../../components/OvalButton';


export default class UpdateTaskContainer extends React.Component{

    state = {
        title: this.props.infoTask.title,
        goal: this.props.infoTask.goal,
        ask: this.props.infoTask.ask,
        difficulty: this.props.infoTask.difficulty,
    };

    //Faz a requisicao para atualizar as informacoes da tarefa
    updateRequest = async () => {
        const token = await AsyncStorage.getItem('TOKEN');

        fetch('https://api-app-focus.herokuapp.com/tasks/' + this.props.infoTask.id, {
            method: "PUT",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+token
            },
            body: JSON.stringify({
                title: this.state.title,
                goal: this.state.goal,
                ask: this.state.ask,
                conclusion: this.state.conclusion,
                difficulty: this.state.difficulty,
            }),
        })
        this.props.navigation.navigate('Task',  {
            topicId: this.props.navigation.state.params.topicId
        });
    }

    //Valida o valor de dificuldade escolhido pelo usuario
    setDificulty = (value) => {
        if(value == null){ //Se nenhuma dificuldade foi selecionada
            this.setState({difficulty: 0});
        }else{
            this.setState({difficulty: value});
        }
    };

    render(){
        return(
            <ScrollView style={styles.view}>
                <View style={styles.viewTitle}>
                    <Text style={styles.title}>Atualizar Tarefa</Text>
                </View>
                <View style={styles.viewInput}>
                    <TextInput
                        style={styles.textInput}
                        name="text" 
                        onChangeText={(text) => this.setState({title: text})} 
                        placeholder="Título"
                        value={this.state.title}
                    />
                </View>

                <View style={styles.viewInput}>
                    <TextInput
                        style={styles.textInput}
                        name="text" 
                        onChangeText={(text) => this.setState({goal: text})} 
                        placeholder="Objetivo"
                        value={this.state.goal}
                    />
                </View>

                <View style={styles.viewInput}>
                    <TextInput
                        style={styles.textInput}
                        name="text" 
                        onChangeText={(text) => this.setState({ask: text})} 
                        placeholder="Pergunta"
                        value={this.state.ask}
                    />
                </View>

                <View style={styles.viewInput}>
                    <RNPickerSelect
                        onValueChange={(value) => this.setDificulty(value)}
                        items={[
                            { label: 'Fácil', value: 0 },
                            { label: 'Médio', value: 1 },
                            { label: 'Difícil', value: 2 },
                        ]}
                        placeholder={{
                            label: "Selecione uma dificuldade",
                            value: null,
                        }}
                        style={{margin: 5}}
                        value={this.state.difficulty}
                    />
                </View>

                <OvalButton onPress={this.updateRequest} text={"Atualizar Tarefa"} buttonWidth={250} />                

            </ScrollView>
        );
    }
}
