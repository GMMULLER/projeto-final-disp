import React from 'react';
import { View, Text, ScrollView, TouchableOpacity } from 'react-native';
import { AsyncStorage } from 'react-native';
import styles, {style_underline_text} from './style';
import Swipeout from 'react-native-swipeout';
import AddItemButton from '../../components/AddItemButton';

export default class TaskContainer extends React.Component {

    constructor(props){
        super(props);
        this.state = { isLoading: true }
    }

    state = {
        loading: true,
        error: false,
        tasks: [],
    };

    //Adiciona um listener que atualizara as tarefas sempre que essa tela estiver em foco
    componentDidMount(){
      this.setState({
        isLoading: true
      });
      this.focusListener = this.props.navigation.addListener('didFocus', () => this.getTasksRequest());
    }

    //Remove o listener
    componentWillUnmount(){
      this.focusListener.remove();
    }

    //Função que retorna todas as tarefas do assunto que o usuário clicou
    getTasksRequest = async () => {
      try {
        const token = await AsyncStorage.getItem('TOKEN');

        return fetch('https://api-app-focus.herokuapp.com/index_tasks/'+this.props.navigation.state.params.topicId, { headers: {"Authorization": "Bearer " + token, "Content-Type":"application/json" }})
          .then((response) => response.json())
          .then((responseJson) => {
            this.setState({
              isLoading: false,
              dataSource: responseJson,
            }, function(){
  
            });
  
          })
          .catch((error) =>{
            console.error(error);
          });
      } catch (error) {
        console.log(error);
      }
    }

    //Faz a requisicao para excluir uma tarefa especifica
    deleteRequest = async (taskId) => {
      try {
        const token = await AsyncStorage.getItem('TOKEN');

        fetch('https://api-app-focus.herokuapp.com/tasks/' + taskId, {
            method: "DELETE",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer " + token
            },
        })
        .then((response) => {
          this.getTasksRequest(); //Da um refresh nas tarefas
        })
        .catch((error) =>{
          console.error(error);
        });
      } catch (error) {
        console.log(error);
      }
    }

    //Trasnforma um valor inteiro da dificuldade em uma string
    transform_difficulty_to_string = (difficulty) => {
      if(difficulty == 0) {
        return "Fácil"
      } else if(difficulty == 1) {
        return "Médio"
      } else if(difficulty == 2) {
        return "Difícil"
      }
    }

    //Transforma uma quantia em segundos em uma string legivel
    transform_total_time = (total_time) => {
      if(total_time >= 3600) {
          hours = parseInt(total_time / 3600);
          restHours = total_time % 3600;
          if(restHours >= 60) {
              minutes = parseInt(restHours / 60);
              seconds = restHours % 60;
              return hours + " hora(s), " + minutes + " minuto(s) e " + seconds + " segundo(s)"
          } else {
              seconds = restHours % 60;
              return hours + " hora(s), " + 0 + "minuto(s) e " + seconds + " segundo(s)"
          }
      } else if(total_time >= 60 && total_time < 3600) {
          minutes = parseInt(total_time / 60);
          seconds = total_time % 60;
          return minutes + " minuto(s) e " + seconds + " segundo(s)"
      } else if(total_time >= 0 && total_time < 60) {
          return total_time + " segundo(s)"
      }
    }

    //Verifica se deve renderizar o botao de criacao de uma nova tarefa
    return_create_task_button_validation = () => {
      if(!this.props.navigation.state.params.done) { //Se o assunto nao estiver finalizado
        return(
          <AddItemButton navigation={this.props.navigation} goTo={"NewTask"} buttonContent={"+ Criar Tarefa"} />
        );
      }
    }

    render() {

        if(this.state.isLoading){
            return(
              <View style={{flex: 1, padding: 20}}>
                <Text>Loading...</Text>
              </View>
            )
        }

        return(

            <ScrollView style={{flex: 1, paddingTop: 10}}>
              <View style={styles.viewTopicTitle}>
                <Text style={styles.topicTitle}>{this.props.navigation.state.params.topicTitle}</Text>
              </View>
                {this.state.dataSource.map((task) => {
                    return(
                        <View style={styles.viewTask} key={task.id} >
                          <Swipeout right={
                                [
                                    {
                                      text: 'Editar',
                                      color: '#800080',
                                      backgroundColor: '#F5F4F4',
                                      onPress: () => { this.props.navigation.navigate('UpdateTask', {
                                        id: task.id, 
                                        title: task.title,
                                        goal: task.goal,
                                        ask: task.ask, 
                                        difficulty: task.difficulty,
                                        topicId: this.props.navigation.state.params.topicId
                                      })}
                                    },
                                    {
                                      text: 'Apagar',
                                      color: '#800080',
                                      backgroundColor: '#F5F4F4',
                                      onPress: () => { this.deleteRequest(task.id) }
                                    }
                                ]
                            }
                                backgroundColor='transparent'
                          >
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('ShowTask', {
                              task_id: task.id,
                              task_finished: task.finished,
                              ask: task.ask,
                              topicDone: this.props.navigation.state.params.done
                            })}>
                              <View style={styles.viewText}>
                                <View style={style_underline_text(task.difficulty)}>
                                  <Text style={styles.text}>{task.title}</Text>
                                </View>
                              </View>
                              <View style={styles.viewText}>
                                <View style={style_underline_text(task.difficulty)}>
                                  <Text style={styles.text}>{this.transform_total_time(task.total_time)}</Text>
                                </View>
                              </View>
                            </TouchableOpacity>
                          </Swipeout>
                        </View>
                    );
                  })}
              {this.return_create_task_button_validation()}
            </ScrollView>
        );
    }

}
