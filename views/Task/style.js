import { StyleSheet } from 'react-native';

export const style_underline_text = (difficulty) => {
  if(difficulty == 0) {
    return({
      borderBottomWidth: 3,
      borderBottomColor: '#00FF00'
    })
  } else if(difficulty == 1) {
    return({
      borderBottomWidth: 3,
      borderBottomColor: '#FFCC00'
    })
  } else if(difficulty == 2) {
    return({
      borderBottomWidth: 3,
      borderBottomColor: '#FF0000'
    })
  }
}


export default StyleSheet.create({
  viewTask: {
    justifyContent: 'center',
    marginTop: 3,
    marginBottom: 3,
    backgroundColor: '#F5F4F4',
    height: 60,
  },
  viewText: {
    alignItems: 'center',
    marginLeft: 10,
    marginRight: 10,
  },
  text: {

  },
  underlineText: {
    borderBottomWidth: 2, 
    borderBottomColor: '#800080',
  },
  topicTitle: {
    fontSize: 17,
    color: '#800080',
    fontWeight: 'bold',
  },
  viewTopicTitle: {
    flexDirection: 'column',
    alignItems: 'center',
    margin: 10,
  }
});