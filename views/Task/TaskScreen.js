import React from 'react';
import { View } from 'react-native';
import general from "../../src/styles/general";
import TaskContainer from "./TaskContainer";
import { StyleSheet } from 'react-native';

export default class TaskScreen extends React.Component {

    static navigationOptions = {
        title: 'Tarefas',
    };

    render(){
      return(
        <View style={general.container}>
          <TaskContainer navigation={this.props.navigation} />
        </View>
      );
    }
  
}

const styles = StyleSheet.create({
  buttonPos: {
    position:'absolute',
    bottom:80, 
    right: 20, 
    alignSelf:'flex-end',
  },
});