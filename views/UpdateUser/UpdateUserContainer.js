import React from 'react';
import { View, TextInput, Text } from 'react-native';
import { TextInputMask } from 'react-native-masked-text'; 
import styles from './style';
import {AsyncStorage} from 'react-native';
import OvalButton from '../../components/OvalButton';

export default class UpdateUserContainer extends React.Component{

    //Tranforma uma quantidade em segundos no formato hh:mm:ss
    secondsToMask = (totalSeconds) => {
        hours = Math.floor(totalSeconds / 3600);
        totalSeconds %= 3600;
        minutes = Math.floor(totalSeconds / 60);
        seconds = totalSeconds % 60;
        
        hours = hours.toString();
        minutes = minutes.toString();
        seconds = seconds.toString();
        
        var resp;
        var hoursP;
        var minutesP;
        var minutesP;

        if(hours.length == 1){
            hoursP = "0".concat(hours).concat(":");
        }else{
            hoursP = hours.concat(":");
        }

        resp = hoursP;

        if(minutes.length == 1){
            minutesP = "0".concat(minutes).concat(":");
        }else{
            minutesP = minutes.concat(":");
        }

        resp = resp.concat(minutesP);

        if(seconds.length == 1){
            secondsP = "0".concat(seconds);
        }else{
            secondsP = seconds;
        }

        resp = resp.concat(secondsP);

        return resp;
    };

    state = {
        name: "",
        email: "",
        interval_time: 0,
        duration_time: 0,
        interval_text: "",
        duration_text: "",
    };

    //Tranforma uma mascara de tempo no formato hh:mm:ss em uma quantia em segundos
    maskToSeconds = (text) => {
        var array = text.split(":");
        var hora = parseInt(array[0], 10);
        hora = hora * 60 * 60;
        var min = parseInt(array[1], 10);
        min = min * 60;
        var sec = parseInt(array[2], 10);
        sec = sec;

        var val = hora + min + sec;

        return val;
    };

    //Faz a requisicao para atualizar as informacoes do usuario
    updateRequest = async () => {
        const token = await AsyncStorage.getItem('TOKEN');
        const id = await AsyncStorage.getItem('ID');

        fetch('https://api-app-focus.herokuapp.com/users/' + id, {
            method: "PUT",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+token
            },
            body: JSON.stringify({
                name: this.state.title,
                email: this.state.goal,
                interval_time: this.state.interval_time,
                duration_time: this.state.duration_time
            }),
        })
        this.props.navigation.navigate('Topic'); //Volta para a tela de assuntos
    }

    //Faz a requisicao para atualizar as informacoes do usuario
    getUserInfo = async () => {
        const token = await AsyncStorage.getItem('TOKEN');
        const id = await AsyncStorage.getItem('ID');
        
        fetch('https://api-app-focus.herokuapp.com/users/' + id, {
            method: "GET",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+token,
            },
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({
                name: responseJson.name,
                email: responseJson.email,
                interval_time: responseJson.interval_time,
                duration_time: responseJson.duration_time,
                interval_text: this.secondsToMask(responseJson.interval_time),
                duration_text: this.secondsToMask(responseJson.duration_time),
            })
        })
        .catch((error) =>{
            console.error(error);
        });   
    }

    componentDidMount(){
        this.getUserInfo();
    }

    render(){
        return(
            <View style={styles.view}>
                
                <View style={styles.viewTitle}>
                    <Text style={styles.title}>Atualizar Perfil</Text>
                </View>

                <View style={styles.viewInput}>
                    <TextInput
                        style={styles.form}
                        name="text" 
                        value = {this.state.name}
                        onChangeText={(text) => this.setState({name: text})} 
                        placeholder="Nome"
                    />
                </View>

                <View style={styles.viewInput}>
                    <TextInput
                        style={styles.form}
                        name="text" 
                        value = {this.state.email}
                        onChangeText={(text) => this.setState({email: text})} 
                        placeholder="E-mail"
                    />
                </View>

                <View style={styles.viewInput}>
                    <Text style={styles.formLabel}>Tempo entre Intervalos</Text>
                    <TextInputMask
                        style={styles.form}
                        type={'datetime'}
                        options={{
                            format: 'HH:mm:ss'
                        }}
                        value={this.state.interval_text}
                        onChangeText={(text) => this.setState({interval_time: this.maskToSeconds(text), interval_text: text})} 
                    />
                </View>

                <View style={styles.viewInput}>
                    <Text style={styles.formLabel}>Duração dos Intervalos</Text>
                    <TextInputMask
                        style={styles.form}
                        type={'datetime'}
                        options={{
                            format: 'HH:mm:ss'
                        }}
                        value={this.state.duration_text}
                        onChangeText={(text) => this.setState({duration_time: this.maskToSeconds(text), duration_text: text})} 
                    />
                </View>

                <OvalButton onPress={this.updateRequest} text={"Atualizar"} buttonWidth={250} />



            </View>
        );
    }
}
