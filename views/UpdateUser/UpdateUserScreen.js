import React from 'react';
import { KeyboardAvoidingView } from 'react-native';
import UpdateUserContainer from "./UpdateUserContainer";
import { ScrollView } from 'react-native-gesture-handler';

export default class UpdateUserScreen extends React.Component {
  
  static navigationOptions = {
    title: "Atualizar Perfil",
    drawerLabel: 'Atualizar Perfil',
  };

  render(){
    return(
      <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'column', justifyContent: 'center'}} behavior="padding" keyboardVerticalOffset={100}>
        <ScrollView>
          <UpdateUserContainer navigation={this.props.navigation}/>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }

}