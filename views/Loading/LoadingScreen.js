import React from 'react';
import { View, Text } from 'react-native'; 
import {AsyncStorage} from 'react-native';
import { connect } from "react-redux";
import general from "../../src/styles/general";

class LoadingScreen extends React.Component {

    constructor(props){
        super(props);
        this.dispatch = props.dispatch;        
    }

    state = {
        isLoading: true
    };

    static navigationOptions = {
        title: 'Loading',
    };

    setUser(token, id){
        return{
            type: 'SET_USER',
            id,
            token
        };
    }

    getUserInfo = async () => {
        try {
            const token = await AsyncStorage.getItem('TOKEN');
            const id = await AsyncStorage.getItem('ID');

            if (token !== null && id !== null) { //Checa se o usuario ja esta logado
                this.dispatch(this.setUser(token, id))
                this.navTopic();
            }else{ //Se nao redireciona para a tela de login
                this.navLogin();
            }

        } catch (error) {
          console.log(error);
        }
    };

    //Redireciona para tela de login
    navLogin(){
        this.props.navigation.navigate('Auth');
    }

    //Redireciona para a tela de topicos
    navTopic(){
        this.props.navigation.navigate('App');
    }

    render(){
        if(this.state.isLoading == true){
            this.getUserInfo();
            return (
                <View style={general.container}>
                    <Text>Loading...</Text>
                </View>
            );
        }

        return(
            <View style={general.container}>
                <Text>Algum problema aconteceu...</Text>
            </View>
        );
    }
}

export default connect(state => ({state: state}))(LoadingScreen);