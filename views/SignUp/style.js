import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    view: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center', 
    },
    viewTitle: {
        alignItems: 'center',
    },  
    title: {
        fontSize: 20,
    },
    viewInput: {
        height: 40,
        flexDirection: 'column',
        justifyContent: 'center',
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 10,
        marginRight: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#0000cc',
    },
    form: {
        flex: 1,
    }
});