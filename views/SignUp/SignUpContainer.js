import React from 'react';
import { View, TextInput, Text } from 'react-native';
import { TextInputMask } from 'react-native-masked-text'; 
import { AsyncStorage } from 'react-native';
import { connect } from "react-redux";
import styles from './style';
import OvalButton from '../../components/OvalButton';

class SignUpContainer extends React.Component {

    constructor(props){
        super(props);
        this.dispatch = props.dispatch;        
    }

    state = {
        username: "",
        email: "",
        password: "",
        password_confirmation: "",
        interval_time: 0,
        duration_time: 0,
        interval_time_text: "",
        duration_time_text: ""
    };

    setUser(token, id){
        return{
            type: 'SET_USER',
            id,
            token
        };
    }

    //Faz a requisicao para o cadastro de um novo usuario
    postRequest = () => {
        fetch('https://api-app-focus.herokuapp.com/users', {
            method: "POST",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                name: this.state.username,
                email: this.state.email,
                password: this.state.password,
                password_confirmation: this.state.password_confirmation,
                interval_time: this.state.interval_time,
                duration_time: this.state.duration_time
            }),
        })
        .then((response) => response.json())
        .then((responseJson) => {
            if(responseJson.user == null){
                console.log("Campos preenchidos incorretamente")
                return 
            }
            this.dispatch(this.setUser(responseJson.jwt, responseJson.user.id));
            this.setUserInfo(responseJson.jwt, responseJson.user.id);
        })
        .catch((error) => {
            console.log(error);
        });
    }

    //Persiste as informacoes de sessao do novo usuario
    setUserInfo = async (token, id) => {
        try {
            await AsyncStorage.setItem('TOKEN', String(token));
            await AsyncStorage.setItem('ID', String(id));
        } catch (error) {
            console.log(error);
        }
        this.props.navigation.navigate('Topic');        
    };

    //Converte a string de tempo de intervalo para um valor em segundos
    convertToSecondsI = (text) => {
        this.setState({interval_time_text: text});
        var array = text.split(":");
        var hora = parseInt(array[0], 10);
        hora = hora * 60 * 60;
        var min = parseInt(array[1], 10);
        min = min * 60;
        var sec = parseInt(array[2], 10);
        sec = sec;

        var val = hora + min + sec;

        this.setState({interval_time: val});
    };

    //Converte a string de duracao de intervalo para um valor em segundos
    convertToSecondsD = (text) => {
        this.setState({duration_time_text: text});
        var array = text.split(":");
        var hora = parseInt(array[0], 10);
        hora = hora * 60 * 60;
        var min = parseInt(array[1], 10);
        min = min * 60;
        var sec = parseInt(array[2], 10);
        sec = sec;

        var val = hora + min + sec;

        this.setState({duration_time: val});
    };


  render(){
    return(
        <View style={styles.view}>
            <View style={styles.viewTitle}>
                <Text style={styles.title}>Cadastrar</Text>
            </View>

            <View style={styles.viewInput}>
                <TextInput
                    style={styles.form}
                    name="text" 
                    onChangeText={(text) => this.setState({username: text})}
                    placeholder="Nome"
                />
            </View>

            <View style={styles.viewInput}>
                <TextInput
                    style={styles.form}
                    name="text" 
                    onChangeText={(text) => this.setState({email: text})} 
                    autoCapitalize='none'
                    placeholder="E-mail"
                />
            </View>

            <View style={styles.viewInput}>
                <TextInput
                    style={styles.form}
                    name="text" 
                    onChangeText={(text) => this.setState({password: text})} 
                    secureTextEntry={true}
                    placeholder="Senha"
                />
            </View>

            <View style={styles.viewInput}>
                <TextInput
                    style={styles.form}
                    name="text" 
                    onChangeText={(text) => this.setState({password_confirmation: text})}
                    secureTextEntry={true}
                    placeholder="Confirmar Senha" 
                />
            </View>

            <View style={styles.viewInput}>
                <Text style={styles.formLabel}>Tempo entre cada intervalo das tarefas</Text>
                <TextInputMask
                    style={styles.form}
                    type={'datetime'}
                    options={{
                        format: 'HH:mm:ss'
                    }}
                    value={this.state.interval_time_text}
                    onChangeText={(text) => this.convertToSecondsI(text)} 
                    placeholder="23:23:23"
                />
            </View>

            <View style={styles.viewInput}>
                <Text style={styles.formLabel}>Duração do intervalo nas tarefas</Text>
                <TextInputMask
                    style={styles.form}
                    type={'datetime'}
                    options={{
                        format: 'HH:mm:ss'
                    }}
                    value={this.state.duration_time_text}
                    onChangeText={(text) => this.convertToSecondsD(text)} 
                    placeholder="23:23:23"
                />
            </View>

            <OvalButton onPress={this.postRequest} text={"Cadastrar"} buttonWidth={250} />

        </View>
    );
  }
}

export default connect(state => ({state: state}))(SignUpContainer);
