import React from 'react';
import { View } from 'react-native'; 
import SignUpContainer from './SignUpContainer'
import general from "../../src/styles/general";

export default class SignUpScreen extends React.Component {
  
  static navigationOptions = {
    title: 'Cadastro',
    header: null
  };


  render(){
    return(
      <View style={general.containerCentralized}>
        <SignUpContainer navigation={this.props.navigation}/>
      </View>
    );
  }

}
