import React from 'react';
import { View, TextInput } from 'react-native';
     
export default class TimeInput extends React.Component {

    
    format_time = (stringTime) => {
        if(stringTime.length > 8) {
            stringTime = stringTime.substring(1,10);
            stringTime = stringTime.replace(':','').replace(':','');
            let hora = stringTime.substring(0,2);
            let minuto = stringTime.substring(2,4);
            let segundo = stringTime.substring(4,6);
            let newStringTime = hora+":"+minuto+":"+segundo;
            return newStringTime;
        }
    }

    save_time = (text) => {
        newText = this.format_time(text);
        this.setState({
            amount_planned_seconds_per_week_text: newText
        });
        this.convertToSecondsI(newText);
    }


    render() {
        return(
            <View style={styles.viewInput}>
                <TextInput
                    style={styles.input}
                    value={this.state.amount_planned_seconds_per_week_text}
                    onChangeText={(text) => this.save_time(text)} 
                    placeholder="00:00:00"
                    maxLength={9}
                    keyboardType="number-pad"
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    viewInput: {
        height: 40,
        flexDirection: 'column',
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#800080',
    },
    input: {
        flex: 1,
    }
});