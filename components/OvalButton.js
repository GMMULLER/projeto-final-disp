import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';

export default class OvalButton extends React.Component {
    
    //Calcula as margens do botao responsivamente
    size_button = () => {
        var {height, width} = Dimensions.get('window');
        var largura = this.props.buttonWidth;
        var tamMargin = parseInt((width - largura) / 2);
        
        //Validação para caso o tamanho da margem seja negativo, ele tenha uma pequena margenzinha e o botão fique "grande"
        if (tamMargin <= 0) {
            tamMargin = 10;
        }
        return {
                marginRight: tamMargin,
                marginLeft: tamMargin,
            }
    }

    render() {
        return(
            <View style={styles.view}>
                <TouchableOpacity
                    style={[styles.button, this.size_button()]}
                    onPress={this.props.onPress}
                >
                    <Text style={styles.text}>{this.props.text}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    view: {
        alignItems: 'center', 
        flexDirection: 'row'
    },
    button: {
        flex: 1,
        borderWidth:1,
        borderColor:'rgba(0,0,0,0.2)',
        borderRadius: 20,
        alignItems:'center',
        justifyContent:'center',
        height:50,
        backgroundColor:'#800080',
        marginTop: 3,
        marginBottom: 3,
    },
    text: {
        color: '#fff'
    }
});