import React from 'react';
import { View, StyleSheet } from 'react-native';

export default class ProgressBar extends React.Component {

    /* 
    PROPS: 
    progressNumber = número inteiro de 1 a 3, repesentando quantas barras o componente terá e a cor das barras
    */

    render_component = (progressNumber) => {
        if(progressNumber == 1) {
            return(
                <View style={styles.view}>
                    <View style={{flex: 0.33, backgroundColor: '#00FF00', marginLeft: 2, marginRight: 2}}>
                    </View>
                    <View style={{flex: 0.33, backgroundColor: '#F5F4F4', marginLeft: 2, marginRight: 2}}>
                    </View>
                    <View style={{flex: 0.33, backgroundColor: '#F5F4F4', marginLeft: 2, marginRight: 2}}>
                    </View>
                </View>
            );
        } else if(progressNumber == 2) {
            return(
                <View style={styles.view}>
                    <View style={{flex: 0.33, backgroundColor: '#FFCC00', marginLeft: 2, marginRight: 2}}>
                    </View>
                    <View style={{flex: 0.33, backgroundColor: '#FFCC00', marginLeft: 2, marginRight: 2}}>
                    </View>
                    <View style={{flex: 0.33, backgroundColor: '#F5F4F4', marginLeft: 2, marginRight: 2}}>
                    </View>
                </View>
            );
        } else if(progressNumber == 3) {
            return(
                <View style={styles.view}>
                    <View style={{flex: 0.33, backgroundColor: '#FF0000', marginLeft: 2, marginRight: 2}}>
                    </View>
                    <View style={{flex: 0.33, backgroundColor: '#FF0000', marginLeft: 2, marginRight: 2}}>
                    </View>
                    <View style={{flex: 0.33, backgroundColor: '#FF0000', marginLeft: 2, marginRight: 2}}>
                    </View>
                </View>
            );
        }
    }

    render() {
        return(
            <View>
                {this.render_component(this.props.progressNumber)}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    view: {
        width: 60, 
        height: 20,
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: 'transparent',
        borderRadius: 20,
        paddingLeft: 5,
        paddingRight: 5,
    }
});