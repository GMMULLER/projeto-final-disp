import React from 'react';
import { View, Image, StyleSheet } from 'react-native';

export default class Logo extends React.Component {
    render() {
        return(
            <View style={styles.positionCenter}>
                <Image 
                    source={require('../src/images/logo.png')}
                    resizeMode="cover"
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    positionCenter: {
        flexDirection: 'column', 
        alignItems: 'center',
    }
});