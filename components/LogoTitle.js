import React from 'react';
import { View, Image, StyleSheet } from 'react-native';

export default class LogoTitle extends React.Component {
    render() {
        return(
            <View style={styles.positionCenter}>
                <Image 
                    source={require('../src/images/logo.png')}
                    resizeMode="cover"
                    style={styles.image}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    positionCenter: {
        flexDirection: 'column', 
        alignItems: 'center',
        alignSelf: "center", 
        marginLeft: "auto", 
        marginRight: "auto"
    },
    image: {
        width: 105,
        height: 45,
    }
});