import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';

export default class AddItemButton extends React.Component {
    render(){
        return(
            <View style={styles.view}>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => this.props.navigation.navigate(this.props.goTo)}
                >
                    <Text style={styles.text}>{this.props.buttonContent}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    view: {
        alignItems: 'center',
        margin: 10,
    },
    button: {

    },
    text: {
        fontSize: 16,
        color: '#800080',
        fontWeight: 'bold',
    },
});