import React from 'react';
import { View, StyleSheet, ActivityIndicator } from 'react-native';

export default class LoadingComponent extends React.Component {
    render(){
        return(
            <View style={styles.view}>
                <ActivityIndicator size="large" color="#0000ff" animating="true" />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    view: {

    }
});