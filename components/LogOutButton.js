import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { connect } from "react-redux";
import {AsyncStorage} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

class LogOutButton extends React.Component {

    constructor(props){
        super(props);
        this.dispatch = props.dispatch;
    }

    setUser(token, id){
        return{
            type: 'SET_USER',
            id,
            token
        };
    }
    
    //Limpa as informacoes de sessao e volta para tela de login
    unsetUserInfo = async () => {
        try {
            await AsyncStorage.removeItem('TOKEN');
            await AsyncStorage.removeItem('ID');
            this.setUser(null, null);
        } catch (error) {
            console.log(error);
        }
        this.dispatch(this.clearTimer());
        this.props.navigation.navigate('Auth');        
    };

    clearTimer(){
        return{
            type: 'CLEAR_TIMER'
        };
    }

    render(){
        return(
            <View>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => this.unsetUserInfo()}
                >
                    <Icon name="ios-log-out" size={20} style={styles.icons} />
                    <Text style={{color: '#ed5b51', marginTop: 5, marginLeft: 10 }}>LogOut</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        flexDirection: 'row',
        borderTopWidth: 1,
        borderTopColor: '#800080',
        paddingTop: 10,
        paddingLeft: 10
    },
    icons: {
        marginLeft: 6,
        marginRight: 6,
        marginTop: 5,
        marginBottom: 5,
    }
});

export default connect(state => ({timer: state}))(LogOutButton);
