import React from 'react';
import { View, StyleSheet } from 'react-native';
import Timer from './Timer';
import { connect } from "react-redux";

class CronometroGlobal extends React.Component {
    
    //Formata a string de hora
    manipulation_string_h(hour) {
        if(hour < 10)
            return "0" + hour;
        else
            return hour;
    }

    //Formata a string de minuto
    manipulation_string_m(min) {
        if(min < 10)
            return "0" + min;
        else
            return min;
    }

    //Formata a string de segundo
    manipulation_string_s(sec) {
        if(sec < 10)
            return "0" + sec;
        else
            return sec;
    }

    render(){
        return(
            <View style={this.props.timer.running && !this.props.timer.timerFocused ? styles.show_timer : styles.hide_timer}>
                <Timer style={this.props.timer.isInBreak ? styles.interval_timer : styles.normal_timer} h={this.manipulation_string_h(this.props.timer.counter.h)} m={this.manipulation_string_m(this.props.timer.counter.m)} s={this.manipulation_string_s(this.props.timer.counter.s)} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    interval_timer: {
        color: '#cc0000',
        fontSize: 40
    },
    normal_timer: {
        color: '#000000',
        fontSize: 40      
    },
    show_timer: {
        color: '#000000',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    hide_timer: {
        display: 'none'
    }
});

export default connect(state => ({timer: state}))(CronometroGlobal);
