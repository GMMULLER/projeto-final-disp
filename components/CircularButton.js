import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';

export default class CircularButton extends React.Component {
    render(){
        return(
            <View style={this.props.stylesButtonPos}>
                <TouchableOpacity
                    style={styles.button}
                    onPress={() => this.props.navigation.navigate(this.props.goTo)}
                >
                    <Text style={{color: '#fff'}}>{this.props.buttonContent}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    button: {
      borderWidth:1,
      borderColor:'rgba(0,0,0,0.2)',
      alignItems:'center',
      justifyContent:'center',
      width:70,
      height:70,
      backgroundColor:'#800080',
      borderRadius:50,
    },
});