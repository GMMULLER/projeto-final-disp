import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';

export default class Timer extends React.Component {
    render(){
        return(
            <View>
               <Text style={this.props.style}> {this.props.h}:{this.props.m}:{this.props.s} </Text>                
            </View>
        );
    }
}

let ScreenHeight = Dimensions.get("window").height;

const styles = StyleSheet.create({
    text_timer: {
        fontSize: 60,
    }
});