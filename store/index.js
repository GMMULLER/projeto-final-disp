import { createStore } from 'redux';

const INITIAL_STATE = {
    running: false,
    timerFocused: false,
    counter: {
        s: 0,
        m: 0,
        h: 0
    },
    interval: null,
    isInBreak: false,
    breakStartedTime: {
        s: 0,
        m: 0,
        h: 0
    },
    lastBreak: {
        s: 0,
        m: 0,
        h: 0
    },
    noteSystem: {
        notes: [],
        taskId: null
    },
    user: {
        id: null,
        token: null
    }
};

function reducer(state = INITIAL_STATE, action){

    if(action.type == 'SET_TIMER'){
        return{
            ...state,
            running: true,
            interval: action.interval
        };
    }

    if(action.type == 'ATT_TIMER'){
        var timer = state.counter;
        var sec = timer.s;
        var min = timer.m;
        var hour = timer.h;

        sec++;

        //Tratar isso, já que a tarefa pode durar mais de um dia
        if(sec == 59 && min == 59 && hour == 23) {
            sec = 0;
            min = 0;
            hour = 0;
        }
        //Caso os segundos cheguem a 60, acrescenta 1 ao minuto e zera os segundos
        if (sec == 60) {
            min++;
            sec = 0;
        }
        //Caso os minutos cheguem a 60, acrescenta 1 à hora e zera os minutos
        if(min == 60) {
            hour++;
            min = 0;
        }

        return {
            ...state,
            counter: {
                s: sec,
                m: min,
                h: hour
            }
        };
    }

    if(action.type == 'CLEAR_TIMER'){
        clearInterval(state.interval);
        return{
            ...state,
            running: false,
            counter: {
                s: 0,
                m: 0,
                h: 0    
            },
            interval: null,
            isInBreak: false,
            timerFocused: false,
            breakStartedTime: {
                s: 0,
                m: 0,
                h: 0
            },
            lastBreak: {
                s: 0,
                m: 0,
                h: 0
            }
        };
    }

    if(action.type == 'STOP_TIMER'){
        auxInterval = state.interval;
        clearInterval(state.interval);
        return{
            ...state,
            running: false,
            interval: null,
        };
    }

    if(action.type == 'SET_NOTES'){
        return{
            ...state,
            noteSystem: {
                notes: action.remoteNotes,
                taskId: action.taskId
            }
        }
    }

    if(action.type == 'APPEND_NOTE'){
        state.noteSystem.notes.push(action.note);
        return state
    }
    
    if(action.type == 'SET_USER'){
        return{
            ...state,
            user: {
                id: action.id,
                token: action.token
            }
        }
    }

    if(action.type == 'CHECK_BREAK'){
        counterSec = (state.counter.h * 60 * 60) + (state.counter.m * 60) + state.counter.s;        
        if(state.isInBreak){ //Se estiver no meio de um Break
            breakStartedTimeSec = (state.breakStartedTime.h * 60 * 60) + (state.breakStartedTime.m * 60) + state.breakStartedTime.s;
            if(counterSec - breakStartedTimeSec >= action.intervalDurationSec){ //Termina o Break se ja tiver transcorrido o tempo maximo
                return {
                    ...state,
                    isInBreak: false,
                    breakStartedTime: {
                        s: 0,
                        m: 0,
                        h: 0
                    },
                    lastBreak: state.counter
                }
            }
        }else{ //Se nao estiver num Break
            lastBreakSec = (state.lastBreak.h * 60 * 60) + (state.lastBreak.m * 60) + state.lastBreak.s;
            if(counterSec - lastBreakSec >= action.intervalTimeSec){ //Comeca o Break se ja tiver transcorrido o tempo maximo desde o ultimo
                return{
                    ...state,
                    isInBreak: true,
                    breakStartedTime: state.counter
                }
            }
        }
    }

    if(action.type == 'SET_FOCUSED'){
        return{
            ...state,
            timerFocused: true
        }
    }

    if(action.type == 'UNSET_FOCUSED'){
        return{
            ...state,
            timerFocused: false
        }
    }

    return state;
}

const store = createStore(reducer);

export default store;