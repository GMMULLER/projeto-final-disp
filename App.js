import React from 'react';
import { View, SafeAreaView, Text, TouchableOpacity, StyleSheet } from 'react-native'; 
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator, DrawerItems } from 'react-navigation-drawer';
import Icon from 'react-native-vector-icons/Ionicons';
import LogOutButton from "./components/LogOutButton";
import { NewTaskScreen } from "./views/NewTask";
import { NewTopicScreen } from "./views/NewTopic";
import { GraphicScreen } from "./views/Graphic";
import { TopicScreen } from "./views/Topic";
import { TaskScreen } from "./views/Task";
import { UpdateTaskScreen } from "./views/UpdateTask";
import { UpdateUserScreen } from "./views/UpdateUser";
import { TimerScreen } from "./views/Timer";
import ShowTaskScreen from './views/ShowTask/ShowTaskScreen';
import { LoadingScreen } from './views/Loading';
import { LoginScreen } from './views/Login';
import { SignUpScreen } from './views/SignUp';
import { Provider } from 'react-redux';
import store from './store';
import CronometroGlobal from './components/CronometroGlobal';
import LogoTitle from './components/LogoTitle';

const TopicStack = createStackNavigator({
  TopicStackHome: {
    screen: TopicScreen,
    navigationOptions: ({navigation}) => ({
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.openDrawer()}>
          <Icon name="ios-menu" size={25} style={styles.icons}/>
        </TouchableOpacity>
      ),
      headerRight: <View style={{padding:20}}></View>
    })
  }
});

const UpdateUserStack = createStackNavigator({
  UpdateUserHome: {
    screen: UpdateUserScreen,
    navigationOptions: ({navigation}) => ({
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.openDrawer()}>
          <Icon name="ios-menu" size={25} style={styles.icons}/>
        </TouchableOpacity>
      ),
      headerTitle: () => <LogoTitle />,
      headerRight: <View style={{padding:20}}></View>
    })
  }
});

const GraphicStack = createStackNavigator({
  GraphicHome: {
    screen: GraphicScreen,
    navigationOptions: ({navigation}) => ({
      headerLeft: (
        <TouchableOpacity onPress={() => navigation.openDrawer()}>
          <Icon name="ios-menu" size={25} style={styles.icons}/>
        </TouchableOpacity>
      ),
      headerTitle: () => <LogoTitle />,
      headerRight: <View style={{padding:20}}></View>
    })
  }
});

const Drawer = createDrawerNavigator({
  Topic: {
    screen: TopicStack,
    navigationOptions: {
      drawerLabel: 'Página Inicial',
    }
  },
  UpdateUser: {
    screen: UpdateUserStack,
    navigationOptions: {
      drawerLabel: 'Editar Usuario',    
    }
  },
  Graphic: {
    screen: GraphicStack,
    navigationOptions: {
      drawerLabel: 'Gráficos'
    }
  }
},
{
  navigationOptions: {
    header: null
  },
  contentComponent:(props) => (
      <View style={{flex:1}}>
          <SafeAreaView forceInset={{ top: 'always', horizontal: 'never' }}>
              <DrawerItems {...props} />
              <LogOutButton navigation={props.navigation}/>
          </SafeAreaView>
      </View>
  )
});

const AppNavigator = createStackNavigator(
  {
    NewTask: NewTaskScreen,
    NewTopic: NewTopicScreen,
    UpdateTask: UpdateTaskScreen,
    Timer: TimerScreen,
    ShowTask: ShowTaskScreen,
    Task: TaskScreen,
    DrawerNav: Drawer
  },
  {
    initialRouteName: 'DrawerNav',
    defaultNavigationOptions: {
      headerTitle: () => <LogoTitle />,
      headerRight: <View style={{padding:20}}></View>
    }
  }
);

const AuthStack = createStackNavigator({
  Login: LoginScreen,
  SignUp: SignUpScreen
});

const AppContainer = createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading: LoadingScreen,
      App: AppNavigator,
      Auth: AuthStack,
    },
    {
      initialRouteName: 'AuthLoading',
    }
  )
);

export default class App extends React.Component{
  render(){
    return (
      <Provider store={store}>
        <AppContainer />
        <CronometroGlobal />
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  icons: {
    marginLeft: 25,
    marginRight: 6,
    marginTop: 5,
    marginBottom: 5,
  }
});