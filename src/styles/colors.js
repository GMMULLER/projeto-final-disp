// src/styles/colors.js

const colors = {
    header: '#333333',
    primary: '#069',
    background: '#FFF',
    text: '#069',
};
  
export default colors;